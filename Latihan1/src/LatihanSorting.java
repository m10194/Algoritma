import java.util.Arrays;
public class LatihanSorting {
    public static void main(String[] args) {
        int [] a = {4, 85, 7, 1, 0, 36, -5, 48};
        for (int i = 0; i< a.length - 1; i++) {
            for (int j = 0; j < a.length - 1 - i; j++) {
                if (a[j + 1] < a[j]){
                    int temp = a[j];
                    a[j] = a[j+1];
                    a[j + 1] = temp;
                }
            System.out.println(Arrays.toString(a));
            }
        }
    }
}