public class Pertemuan6 {
    public static void main(String[] args) {
        for(int i=1;i<=10;i+=1){
            System.out.println(i);
        }
        System.out.println("=================");
        for(int i=1;i<=10;i+=2){
            System.out.println(i);
        }
        System.out.println("=================");
        for(int i=1;i<=10;i++){
            if(i % 2 == 0){
                System.out.println("Angka "+i+" adalah genap");
            }else{
                System.out.println("Angka "+i+" adalah ganjil");
            }
        }
    }
}
